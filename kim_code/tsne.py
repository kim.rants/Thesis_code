import os
import pandas as pd
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import numpy as np
import sys
sys.path.append("/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/gbm-mutations/kim_code/")
import radiogenomics as r


"""
Program: Download data, run t-SNE, store data in csv
"""

"""
Section: download data and clean it
"""
# Download and clean set up dataFrame:
df_TCGA = r.download_data("TCGA")
df_TCGA = r.make_clean_df("TCGA", df_TCGA)

# Download and clean set up dataFrame:
df_TCIA = r.download_data("TCIA")
df_TCIA = r.make_clean_df("TCIA", df_TCIA)

# List of matches / overlap
matches = r.find_matches(df_TCIA, df_TCGA)

# Create dataframe with only the matches
df_TCGA_only_matches = pd.DataFrame(data=df_TCGA, index=matches)
df_TCIA_only_matches = pd.DataFrame(data=df_TCIA, index=matches)

# Set Column and index names (make pretty)
df_TCGA_only_matches.index.names = ['ID']  # Set index
df_TCGA_only_matches.columns.names = ['Mutations']  # Column name
df_TCIA_only_matches.index.names = ['ID']  # Set index
df_TCIA_only_matches.columns.names = ['Features']  # Column name


"""
Section: Prepare data for PCA/t-SNE
"""

# Get primary and secondary again with ID as index
df_type_with_index = r.get_countplot_data('GBM_TYPE', df_TCGA=df_TCGA_only_matches)

#Merge
df_TCIA_matches_and_class = df_TCIA_only_matches.join(df_type_with_index)

# y-label: Primary vs. secondary GBM
# X-label: all imaging features
X = df_TCIA_matches_and_class.drop(axis=1, labels='GBM Type').values # Everything but class, just values
y_labels = df_TCIA_matches_and_class['GBM Type'].values # Just the class, just values

# Standardise X
X_std = StandardScaler().fit_transform(X)


"""
Section: PCA + t-SNE
"""
# Pick only K of the D-dimensions; k = 15
DIMENSIONS = 40
pca_results = r.run_pca(X_std, DIMENSIONS)


# Loop and store results
path = '/Users/kimrants/Desktop/tsne/'

for i in range(1,11):
    perplexity = (i*5)
    tsne_results = r.run_tsne(pca_results,
                            dimensions=2,
                            perplexity=perplexity,
                            learning_rate=75)

    df = pd.DataFrame(data=tsne_results,
                      columns=['x', 'y'],
                      index=df_TCIA_matches_and_class.index)


    df.to_csv(path+'perplexity'+str(perplexity), encoding='utf-8', index=False)

