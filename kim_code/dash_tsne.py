# DASH
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output  # for callback
import plotly.graph_objs as go
import pandas as pd
import sys

sys.path.append("/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/gbm-mutations/kim_code/")
import radiogenomics as r
import numpy as np

# Global Var
subject = ""


################## BUILD APP ##################
app = dash.Dash()
app.config['suppress_callback_exceptions'] = True
app.css.append_css({"external_url": "/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/kim_code/undo_buton.css"})

################## DOWNLOAD DATA ##################
bakas_PCs = ['NO', '13', '19', '29', '38']
own_PCs = ['NO', '9', '12', '18', '23']
bakas_large_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/Bakas57subjects/280genes_PCimg/'
bakas_small_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/Bakas57subjects/9genes_11img/'
own_large_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/OwnFeaturesNorm/280genes_3900img/'
own_small_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/OwnFeaturesNorm/9genes_11img/'


def get_dict_tsne_files(PCs, path):
    d = {}

    for pc in PCs:
        for dim in r.tsne_dims:
            for perp in r.perps:
                for learn in r.learn_rates:
                    # Read file
                    d["PCs{}Dim{}Perp{}Learn{}".format(pc, dim, perp, learn)] = pd.read_csv(
                        path + "tsne_PCs{}_dim{}_perp{}_learnRate{}".format(pc, dim, perp, learn))

                    # Set Index
                    d["PCs{}Dim{}Perp{}Learn{}".format(pc, dim, perp, learn)].set_index(
                        d["PCs{}Dim{}Perp{}Learn{}".format(pc, dim, perp, learn)].columns[0],
                        inplace=True)
    return d


# Pipeline: Bakas Large
bakas_large_dict = get_dict_tsne_files(bakas_PCs, bakas_large_path)
bakas_large_tsne_scores = pd.read_csv(
    '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/Bakas57subjects/280genes_PCimg/T-SNE_DF_DATA')
bakas_large_tsne_scores = bakas_large_tsne_scores.set_index('Unnamed: 0', drop=True)
bakas_large_nearest_neighbours = np.load(
    '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/Bakas57subjects/280genes_PCimg/k_thres_neighbours.npy').item()

# Pipeline: Bakas Small
bakas_small_dict = get_dict_tsne_files(['NO'], bakas_small_path)
bakas_small_tsne_scores = pd.read_csv(
    '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/Bakas57subjects/9genes_11img/T-SNE_DF_DATA')
bakas_small_tsne_scores = bakas_small_tsne_scores.set_index('Unnamed: 0', drop=True)
bakas_small_nearest_neighbours = np.load(
    '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/Bakas57subjects/9genes_11img/k_thres_neighbours.npy').item()

# Pipeline: Own Large
own_large_dict = get_dict_tsne_files(own_PCs, own_large_path)
own_large_tsne_scores = pd.read_csv(
    '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/OwnFeaturesNorm/280genes_3900img/T-SNE_DF_DATA')
own_large_tsne_scores = own_large_tsne_scores.set_index('Unnamed: 0', drop=True)
own_large_nearest_neighbours = own_small_nearest_neighbours = np.load(
    '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/OwnFeaturesNorm/280genes_3900img/k_thres_neighbours.npy').item()

# Pipeline: Own Small
own_small_dict = get_dict_tsne_files(['NO'], own_small_path)
own_small_tsne_scores = pd.read_csv(
    '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/OwnFeaturesNorm/9genes_11img/T-SNE_DF_DATA')
own_small_tsne_scores = own_small_tsne_scores.set_index('Unnamed: 0', drop=True)
own_small_nearest_neighbours = np.load(
    '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/OwnFeaturesNorm/9genes_11img/k_thres_neighbours.npy').item()

# Get gene groups
feat_df = pd.read_csv('/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/gene_groups_list')
feat_df = feat_df.set_index('Unnamed: 0', drop=True)
feature_list = list(feat_df.columns)





############### APP LAYOUT ##################
app.title = 'Radiogenomic Data Visualisation'
app.layout = html.Div(children=[

    html.H1('Dashboard for Visualisation of Imaging Features',
            style={
                'textAlign': 'center',
                'color': 'black'}),

    dcc.Tabs(tabs=[
        {'label': 'Pre-supplied (9 genes, 11 img.)', 'value': 'BakasSmall'},
        {'label': 'Pre-supplied (278 genes, 704 img.)', 'value': 'BakasLarge'},
        {'label': 'Extracted (9 genes, 11 img.)', 'value': 'OwnSmall'},
        {'label': 'Extracted (278 genes, 3246 img.)', 'value': 'OwnLarge'}],
        value='BakasLarge',
        id='tabs'),

    html.Label('Pick Genes'),
    dcc.Dropdown(id='gene-Dropdown',
                 options=[{'label': s, 'value': s}
                          for s in feature_list],
                 multi=True),

    html.Label('Overlay Genomic Neighbours'),
    html.Div(id='overlay_neighbours'),
    html.Div([dcc.RadioItems(id='neighbours',
                             options=[
                                 {'label': 'Yes', 'value': 'Yes'},
                                 {'label': 'No', 'value': 'No'}],
                             value=None)]),

    html.Div(id='output'),

    html.Div([
        html.Div([
            html.Label('Dimensions in plot'),
            dcc.RadioItems(id='dimensions',
                           options=[
                               {'label': '2D', 'value': 2},
                               {'label': '3D', 'value': 3},
                               {'label': '3x2D', 'value': 4}],
                           value=2),

            html.Br(),

            html.Div([html.Label('Number of Principal Components'),
                      dcc.Slider(id='slider_pca_components',
                                 min=-1,
                                 max=5,
                                 step=None,
                                 value=0,
                                 marks={
                                     0: 'Original Variables',
                                     4: '95%',
                                     3: '90%',
                                     2: '80%',
                                     1: '70%'},
                                 updatemode='drag'
                                 )]),
            html.Br(),

            html.Div([html.Label('Perplexity'),
                      dcc.Slider(id='slider',
                                 min=-5,
                                 max=55,
                                 step=None,
                                 value=20,
                                 marks={
                                     5: '5',
                                     10: '10',
                                     15: '15',
                                     20: '20',
                                     25: '25',
                                     30: '30',
                                     35: '35',
                                     40: '40',
                                     45: '45',
                                     50: '50'},
                                 updatemode='drag'
                                 )]),
            html.Br(),

            html.Div([html.Label('Learning Rate'),
                      dcc.Slider(
                          id='slider_learn_rate',
                          min=-100,
                          max=1100,
                          step=None,
                          value=50,
                          marks={
                              r.learn_rates[0]: str(r.learn_rates[0]),
                              r.learn_rates[1]: str(r.learn_rates[1]),
                              r.learn_rates[2]: str(r.learn_rates[2]),
                              r.learn_rates[3]: str(r.learn_rates[3]),
                              r.learn_rates[4]: str(r.learn_rates[4]),
                              r.learn_rates[5]: str(r.learn_rates[5]),
                              r.learn_rates[6]: str(r.learn_rates[6]),
                              r.learn_rates[7]: str(r.learn_rates[7])
                          },
                          updatemode='drag')]),

        ], style={'flexGrow': '1'}),

        html.Div([

            html.Div([  # html.Label('Threshold'),

                html.Label('Find optimal hyperparameters for specified k and threshold?'),
                html.Div([
                    dcc.RadioItems(id='optimise',
                                   options=[
                                       {'label': 'Yes', 'value': 'Yes'},
                                       {'label': 'No', 'value': 'NO'}],
                                   value='NO')]
                ),

                html.Div(id='out_opt'),

                html.Br(),

                html.Div([html.Label('K-neighbours'),
                          dcc.Slider(id='k',
                                     min=-0,
                                     max=12,
                                     step=None,
                                     value=5,
                                     marks={
                                         2: 2,
                                         5: 5,
                                         8: 8,
                                         10: 10},
                                     updatemode='drag'
                                     )]),
                html.Br(),
                html.Br(),

                dcc.Dropdown(id='threshold',
                             options=[
                                 {'label': 'Threshold = 2', 'value': 2},
                                 {'label': 'Threshold = 3', 'value': 3},
                                 {'label': 'Threshold = 4', 'value': 4},
                                 {'label': 'Threshold = 5', 'value': 5},
                                 {'label': 'Threshold = 10', 'value': 10},
                                 {'label': 'Threshold = 20', 'value': 20},
                                 {'label': 'Threshold = 100', 'value': 100}
                             ],
                             value=100
                             )]),

        ], style={'flexGrow': '1'})

    ], style={'display': 'flex'}),



])


### Find optimal parameters
@app.callback(
    Output(component_id='out_opt', component_property='children'),
    [Input(component_id='optimise', component_property='value'),
     Input(component_id='tabs', component_property='value'),
     Input(component_id='threshold', component_property='value'),
     Input(component_id='k', component_property='value')])
def optimal_hypers(optimise, input_tabs, thres, k):
    if optimise == 'NO':
        return ""
    else:
        if input_tabs == 'BakasSmall':
            tsne_score = bakas_small_tsne_scores

        elif input_tabs == 'BakasLarge':
            tsne_score = bakas_large_tsne_scores

        elif input_tabs == 'OwnSmall':
            tsne_score = own_small_tsne_scores

        elif input_tabs == 'OwnLarge':
            tsne_score = own_large_tsne_scores

    df = tsne_score
    min_score = df.loc[(df['Threshold'] == thres) & (df['k'] == k)]['Score'].min()
    perp = df.loc[(df['Threshold'] == thres) & (df['k'] == k) & (df['Score'] == min_score)]['Perplexity']
    learn_rate = df.loc[(df['Threshold'] == thres) & (df['k'] == k) & (df['Score'] == min_score)]['Learning Rate']
    pc = df.loc[(df['Threshold'] == thres) & (df['k'] == k) & (df['Score'] == min_score)]['Principal Components']
    dim = df.loc[(df['Threshold'] == thres) & (df['k'] == k) & (df['Score'] == min_score)]['t-SNE dimensions']

    return "t-SNE dim: {},    PCs: {},    Perplexity: {},    Learning Rate: {}".format(dim.values[0],
                                                                                       pc.values[0],
                                                                                       perp.values[0],
                                                                                       learn_rate.values[0])


@app.callback(
    Output(component_id='output', component_property='children'),
    [
        Input(component_id='slider_pca_components', component_property='value'),
        Input(component_id='slider', component_property='value'),
        Input(component_id='slider_learn_rate', component_property='value'),
        Input(component_id='gene-Dropdown', component_property='value'),
        Input(component_id='neighbours', component_property='value'),
        Input(component_id='threshold', component_property='value'),
        Input(component_id='dimensions', component_property='value'),
        Input(component_id='tabs', component_property='value'),
        Input(component_id='k', component_property='value')])
def update_graph(pc, perp, learn_rate, input_genes, neighbours, threshold, input_dimensions, input_tabs, k):
    # Rewrite gene groups into simple numbers (1,2,3,4) so it works with all the files... Hack.
    if input_genes is not None:
        for i in range(len(input_genes)):
            if "Group " in input_genes[i]:
                input_genes[i] = input_genes[i][6:8]  # Get group number
                if input_genes[i][1] == ":":
                    input_genes[i] = input_genes[i][0]

    # Get right data
    if input_tabs == 'BakasSmall':
        d = bakas_small_dict
        PC = bakas_PCs[0]  # only 'NO'
        tsne_score_data = bakas_small_tsne_scores
        nearest_neighbours_dict = bakas_small_nearest_neighbours


    elif input_tabs == 'BakasLarge':
        d = bakas_large_dict
        PC = bakas_PCs[pc]
        tsne_score_data = bakas_large_tsne_scores
        nearest_neighbours_dict = bakas_large_nearest_neighbours


    elif input_tabs == 'OwnSmall':
        d = own_small_dict
        PC = own_PCs[0]  # only 'NO'
        tsne_score_data = own_small_tsne_scores
        nearest_neighbours_dict = own_small_nearest_neighbours

    elif input_tabs == 'OwnLarge':
        d = own_large_dict
        PC = own_PCs[pc]
        tsne_score_data = own_large_tsne_scores
        nearest_neighbours_dict = own_large_nearest_neighbours

    # Because the value can be 4.... need to treat this for looking up score.
    if input_dimensions == 2:
        dim = 2
    else:
        dim = 3
    # Make string column to allow for comparison
    tsne_score_data['Principal Components'] = tsne_score_data['Principal Components'].astype(str)
    score = list(tsne_score_data.loc[(tsne_score_data['t-SNE dimensions'] == dim) &
                                     (tsne_score_data['Learning Rate'] == learn_rate) &
                                     (tsne_score_data['Threshold'] == threshold) &
                                     (tsne_score_data['Principal Components'] == PC) &
                                     (tsne_score_data['Perplexity'] == perp) &
                                     (tsne_score_data['k'] == k)
                                     ]['Score'])[0]

    ###########--------- NEED TRIPLE IF TO EITHER PLOT IN 2D, 3D or 3x2D -----------#########

    if input_dimensions == 2:

        #  Make DataFrame based on gene-dropdown
        df_selected_hyper = d["PCs{}Dim{}Perp{}Learn{}".format(PC, input_dimensions, perp, learn_rate)]
        df_coords = df_selected_hyper[['x', 'y']]
        df_all_features = df_selected_hyper.drop(['x', 'y'], axis=1)
        df = r.helper_make_other_df(df_all_features, input_genes)

        # Update DataFrame to also include neighbours and focus_subject
        if neighbours == 'Yes':  # NO
            df = df.rename(columns={'All IDs': 'Other IDs'})
            my_list = list(
                nearest_neighbours_dict['k={}_thres={}'.format(k, threshold)].loc[subject])  # Global variable
            my_col = []
            focus_subject = []
            for i in (df.index):
                if i in my_list:
                    my_col.append(1)
                else:
                    my_col.append(0)

                if i == subject:
                    focus_subject.append(1)
                else:
                    focus_subject.append(0)
            df['Neighbours'] = my_col
            df['Subject in focus'] = focus_subject

        traces = []

        # Create traces
        for feature in df.columns:
            indeces = df[df[feature] > 0]
            X = df_coords.loc[indeces.index, 'x']
            Y = df_coords.loc[indeces.index, 'y']
            ID = df_coords.loc[indeces.index].index
            if feature in r.gene_groups:
                vals = df.loc[indeces.index, feature]
            else:
                vals = []

            traces.append(go.Scatter(
                x=X,
                y=Y,
                text=ID,  # Display ID
                mode='markers',
                opacity=0.7,
                marker={'size': r.marker_size(vals),  # Create Marker size
                        'line': {'width': 0.5,
                                 'color': 'white'}},
                name=feature))

        return html.Div([dcc.Graph(id='graph',
                                   figure={
                                       'data': traces,
                                       'layout': go.Layout(
                                           title="Score is {0:.3f}".format(score),
                                           xaxis={'title': 'X'},
                                           yaxis={'title': 'Y'},
                                           hovermode='closest')})])

    if input_dimensions == 3:

        #  Make DataFrame based on gene-dropdown
        df_selected_perplexity = d["PCs{}Dim{}Perp{}Learn{}".format(PC, input_dimensions, perp, learn_rate)]
        df_coords = df_selected_perplexity[['x', 'y', 'z']]
        df_all_features = df_selected_perplexity.drop(['x', 'y', 'z'], axis=1)
        df = r.helper_make_other_df(df_all_features, input_genes)

        # Update DataFrame to also include neighbours and focus_subject
        if neighbours == 'Yes':  # NO
            df = df.rename(columns={'All IDs': 'Other IDs'})  # Just change name
            my_list = list(
                nearest_neighbours_dict['k={}_thres={}'.format(k, threshold)].loc[
                    subject])  # Global variable, pick neighbours from subject
            my_col = []
            focus_subject = []
            for i in (df.index):
                if i in my_list:
                    my_col.append(1)  # Mark Neighbours
                else:
                    my_col.append(0)

                if i == subject:
                    focus_subject.append(1)  # Mark focus object
                else:
                    focus_subject.append(0)
            df['Neighbours'] = my_col
            df['Subject in focus'] = focus_subject

        traces = []

        # Create traces
        for feature in df.columns:
            indeces = df[df[feature] > 0]
            X = df_coords.loc[indeces.index, 'x']
            Y = df_coords.loc[indeces.index, 'y']
            Z = df_coords.loc[indeces.index, 'z']
            ID = df_coords.loc[indeces.index].index
            if feature in r.gene_groups:
                vals = df.loc[indeces.index, feature]
            else:
                vals = []

            traces.append(go.Scatter3d(
                x=X,
                y=Y,
                z=Z,
                text=ID,
                mode='markers',
                opacity=0.7,
                marker={'size': r.marker_size(vals),
                        'line': {'width': 0.5,
                                 'color': 'white'}},
                name=feature))

        return html.Div([dcc.Graph(id='graph',
                                   figure={
                                       'data': traces,
                                       'layout': go.Layout(
                                           title="Score is {0:.3f}".format(score),
                                           scene=go.Scene(
                                               xaxis={'title': 'X'},
                                               yaxis={'title': 'Y'},
                                               zaxis={'title': 'Z'}
                                           ),
                                           hovermode='closest')})])

    if input_dimensions == 4:
        tsne_dim = 3  # to pick right plot

        #  Make DataFrame based on gene-dropdown
        df_selected_perplexity = d["PCs{}Dim{}Perp{}Learn{}".format(PC, tsne_dim, perp, learn_rate)]
        df_coords = df_selected_perplexity[['x', 'y', 'z']]
        df_all_features = df_selected_perplexity.drop(['x', 'y', 'z'], axis=1)
        df = r.helper_make_other_df(df_all_features, input_genes)

        # Update DataFrame to also include neighbours and focus_subject
        if neighbours == 'Yes':  # NO
            df = df.rename(columns={'All IDs': 'Other IDs'})  # Just change name
            my_list = list(
                nearest_neighbours_dict['k={}_thres={}'.format(k, threshold)].loc[
                    subject])  # Global variable, pick neighbours from subject
            my_col = []
            focus_subject = []
            for i in (df.index):
                if i in my_list:
                    my_col.append(1)  # Mark Neighbours
                else:
                    my_col.append(0)

                if i == subject:
                    focus_subject.append(1)  # Mark focus object
                else:
                    focus_subject.append(0)
            df['Neighbours'] = my_col
            df['Subject in focus'] = focus_subject

        tracesX_Y = []
        tracesX_Z = []
        tracesY_Z = []

        # Create traces
        for feature in df.columns:
            indeces = df[df[feature] > 0]
            X = df_coords.loc[indeces.index, 'x']
            Y = df_coords.loc[indeces.index, 'y']
            Z = df_coords.loc[indeces.index, 'z']
            ID = df_coords.loc[indeces.index].index
            if feature in r.gene_groups:
                vals = df.loc[indeces.index, feature]
            else:
                vals = []

            # X - Y
            tracesX_Y.append(go.Scatter(
                x=X,
                y=Y,
                text=ID,
                mode='markers',
                opacity=0.7,
                marker={'size': r.marker_size(vals),
                        'line': {'width': 0.5,
                                 'color': 'white'}},
                name=feature))

            # X - Z
            tracesX_Z.append(go.Scatter(
                x=X,
                y=Z,  # Z
                text=ID,
                mode='markers',
                opacity=0.7,
                marker={'size': r.marker_size(vals),
                        'line': {'width': 0.5,
                                 'color': 'white'}},
                name=feature))

            # Y - Z
            tracesY_Z.append(go.Scatter(
                x=Y,  # Y
                y=Z,  # Z
                text=ID,
                mode='markers',
                opacity=0.7,
                marker={'size': r.marker_size(vals),
                        'line': {'width': 0.5,
                                 'color': 'white'}},
                name=feature))

        return html.Div([dcc.Graph(id='graph',
                                   figure={
                                       'data': tracesX_Y,
                                       'layout': go.Layout(
                                           title="",
                                           xaxis={'title': 'X'},
                                           yaxis={'title': 'Y'},
                                           hovermode='closest')}),
                         dcc.Graph(id='graph2',
                                   figure={
                                       'data': tracesX_Z,
                                       'layout': go.Layout(
                                           title="Score is {0:.3f}".format(score),
                                           xaxis={'title': 'X'},
                                           yaxis={'title': 'Z'},
                                           hovermode='closest')}),
                         dcc.Graph(id='graph3',
                                   figure={
                                       'data': tracesY_Z,
                                       'layout': go.Layout(
                                           title="",
                                           xaxis={'title': 'Y'},
                                           yaxis={'title': 'Z'},
                                           hovermode='closest')})],

                        style={'columnCount': 3})


@app.callback(
    Output(component_id='overlay_neighbours', component_property='children'),
    [
        Input(component_id='graph', component_property='clickData')])
def update_global_var(val):
    global subject
    subject = val['points'][0]['text']
    return val['points'][0]['text']


if __name__ == '__main__':
    app.run_server()
