import dash
import dash_core_components as dcc
import dash_html_components as html
# PLOTLY
import plotly.graph_objs as go

################## BUILD APP ##################
# Build "Flask" application
app = dash.Dash()


colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}


################# APP LAYOUT ##################
# Static part
app.layout = html.Div(children =[   # Children is a list
        html.H1('Hello Dash',   # Header
                style={
                    'textAlign':'center',
                    'color':colors['text']
                }),

        # Add division
        html.Div('Dash something'),

        # Add graph
        dcc.Graph(id='example',
                  figure={
                    'data':[
                        {'x':[1,2,3], 'y':[2,3,4], 'type': 'bar', 'name': 'SF'},
                        {'y':[2,5,7], 'y':[10,11,3], 'type':'bar', 'name': 'NYC'},
                    ],

                    'layout': { # add background color
                        'plot_bgcolor': colors['background'],
                        'paper_bgcolor':colors['background'],
                        'font': {
                            'color': colors['text']
                        },
                        'title':'BAR PLOTS'
                    }
                  })

    ],

    # style to entire DIv
    style= {
        'backgroundColor':colors['background']
    }
)


################# Run Server ##################
if __name__ == '__main__':
    app.run_server()