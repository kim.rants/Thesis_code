import dash
import dash_core_components as dcc
import dash_html_components as html
# PLOTLY
import plotly.graph_objs as go

import pandas as pd


################## BUILD APP ##################
app = dash.Dash()

app.layout = html.Div(['This is the outer most Div!',
                      html.Div('This is an inner Div',
                               style={'color': 'red'}),
                      html.Div('another inner Div',
                               style={'color':'blue',
                                      'border':'2px black solid' })
                       ],
                      style={
                          'color':'green',
                          'border':'2px green solid'
                      }

)


################# Run Server ##################
if __name__ == '__main__':
    app.run_server()