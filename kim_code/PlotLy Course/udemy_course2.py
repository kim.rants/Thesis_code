import dash
import dash_core_components as dcc
import dash_html_components as html
# PLOTLY
import plotly.graph_objs as go

import pandas as pd


################## BUILD APP ##################
app = dash.Dash()


# Data
path = '/Users/kimrants/Desktop/tsne/perplexity'

# Download data, create dictionary with all files in dataFrames (perplexity is 5 to 50)
dict = {}
for i in range(1,11):
    perplexity = i*5
    dict["perp{}".format(perplexity)] = pd.read_csv(path+str(perplexity))

X = dict['perp5']['x']
Y = dict['perp5']['y']

app.layout = html.Div([dcc.Graph(id='scatterplot',
                          figure = { 'data' : [
                                go.Scatter(x=X,
                                    y=Y,
                                    mode='markers',
                                    marker = {
                                        'size': 12,
                                        #'symbol': 'pentagon'
                                    }
                                    )],
                                    'layout': go.Layout(title='My Scatter',
                                                        xaxis={'title':'X axis title'})}
                          )])



if __name__ == '__main__':
    app.run_server()