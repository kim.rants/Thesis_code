import dash
import dash_core_components as dcc
import dash_html_components as html
# PLOTLY
import plotly.graph_objs as go

# Build "Flask" application
app = dash.Dash()

app.layout = html.Div([

    html.Label('Dropdown'),         # NOTICE LABEL
    dcc.Dropdown(
        options=[
            {'label': 'NYC', 'value': 'NYC'},
            {'label': 'SF', 'value': 'SF'}
        ],
        value='SF'  # Default Value
    ),

    html.Label('Slider'),           # Just name above
    dcc.Slider(min=-10, max=10, step=0.5, value=0,
               marks={i: i for i in range(-10,10)}),



    # P-tag gives a paragraph.... solves space issues
    html.P(html.Label('RadioItems')),    #Linked to component after
    dcc.RadioItems(
        options=[
            {'label': 'NYC', 'value': 'NYC'},
            {'label': 'SF', 'value': 'SF'}
        ],
        value='SF'
    )


])

################# Run Server ##################
if __name__ == '__main__':
    app.run_server()
