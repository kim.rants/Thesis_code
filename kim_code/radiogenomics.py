import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import os, six
import plotly.plotly as py
from plotly.graph_objs import *
import plotly.tools as tls
#import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from scipy.spatial import distance_matrix
import plotly.graph_objs as go
import seaborn as sns
# feature extraction
from radiomics import featureextractor


"""
CONSTANTS
"""
TCGA_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/Bakas57subjects/TCGA.GBM.mutect.somaticgene_matrix.csv'
TCIA_path =  '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/Bakas57subjects/TCGA_GBM_radiomicFeatures.txt' # From Guada project
survival_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/Bakas57subjects/tcga_survival_data.csv'
gene_list_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/gene_list.txt'
gene_group_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/Bakas57subjects/IPA_results_RSFgenes.csv'
# Save data

save_own_features = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/OwnFeatures/'


# SAME
dash_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/Bakas57subjects/280genes_PCimg/'
save_data_path = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Output_Data/Bakas57subjects/280genes_PCimg/'



dist_matrix_path ='/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/L1distance_RSFmutationsMODIFIED.csv'
BRATS_TCIA = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/OwnFeatures/BRaTS_TCIA_matches74.xlsx'

# Hyperparameters t-SNE
tsne_dims =[2,3]
PCs = ['NO', 13, 19, 29, 38]
perps = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
learn_rates = [10, 50, 100, 200, 300, 500, 750, 1000]
gene_groups = [str(i) for i in range (1,24)]
thresholds = [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,9999]
Ks = [1, 2, 3, 5, 8, 10, 12, 15, 18, 22, 28, 35, 40, 45, 52]


num_tsne_files = len(tsne_dims)*len(PCs)*len(perps)*len(learn_rates)*len(thresholds)*len(Ks)*2 # 2 is for own & Bakas

# Labels
NECROTIC = 1
EDEMA = 2
ENHANCING_TUMOR = 4

# For Dash (TCIA-TCGA matches WITH survival)
NUM_SUBJECTS_BAKAS = 57

# Coloring
NAVY = 'rgb(0,0,128)'
LIGHT_BLUE = 'rgb(4, 182, 247)'



"""
Download data. Either TCGA, TCIA, GENES_list.
"""
def download_data(type):
    """This is a DOC string!"""

    if type == "TCGA":
        df = pd.read_csv(TCGA_path)

    elif type == "TCIA":
        df = pd.read_csv(TCIA_path)

    elif type == "GENES":
        df = pd.read_csv(gene_list_path)
        top_gene = df.columns[0]
        all_genes = df.iloc[:,0].values.tolist()
        all_genes.append(top_gene)
        df = pd.DataFrame(all_genes, columns=['Selected Genes'])
    elif type == "TCGA_CLEAN":
        df = pd.read_csv(save_data_path + "TCGA_clean")
        df.set_index('ID', inplace=True)

    elif type == "TCIA_CLEAN":
        df = pd.read_csv(save_data_path + "TCIA_clean")
        df.set_index('ID', inplace=True)

    elif type == "BRATS_TCIA_MATCHES":
        df = pd.read_excel(BRATS_TCIA)
        df.set_index('TCGA', inplace=True, drop=True)
    return df

def download_directly(ending_path):
    df = pd.read_csv(save_data_path + ending_path)
    df.set_index('ID', inplace=True)
    return df

def download_directly_own_features(ending_path):
    df = pd.read_csv(save_own_features + ending_path)
    df.set_index('ID', inplace=True)
    return df


"""
Save Data
"""
def save(data, name):
    data.to_csv(save_data_path + name)

def save_ownFeatureFolder(data, name):
    data.to_csv(save_own_features + name)


def save_dict(data, name):
    np.save(save_own_features + name, data)

def load_dict(name):
    read_dictionary = np.load(save_own_features + name + ".npy").item()
    return read_dictionary


def load_dict_replication(name):
    read_dictionary = np.load(save_data_path + name + ".npy").item()
    return read_dictionary


def make_clean_df(type, df):
    """
    :param type: string of "TCIA" or "TCGA"
    :param df: DataFrame, uncleaned
    :return: Clean DataFrame, N as row, D as columns, NaN values dropped
    """
    if type == "TCGA":
        df = df.transpose()             # Transpose data to get (N,D)
        new_header = df.iloc[0]         # Grab first row for the header
        df = df[1:]                     # Take the data less the header row
        df.columns = new_header         # Set header row as df header
        df.index.names = ['ID']         # Set index
        df.columns.names = ['Mutations']# Set column name
    elif type == "TCIA":
        df = df.set_index('ID')         # Pick ID-column as index
        df.index.names = ['ID']         # Set index header name
        df.columns.names = ['Features'] # Set column name
        df = df.drop(['Date'], axis=1)  # Remove date column
        df = df.dropna(axis=1,          # Drop NaN columns
                       how='any',
                       thresh=None,
                       subset=None,
                       inplace=False)
    else:
        print("Input type incorrect")   # React to most common input mistake
    return df



"""
Simple countplot data
type:
- TCIA-TCGA: return column with TCIA/TCGA values only.
- GBM type: return column with prim/sec based on IDH1 gene mutation
"""
def get_countplot_data(type, df_TCIA = None, df_TCGA = None):
    if type == "TCIA-TCGA":
        subject_list = []
        for i in range(df_TCIA.shape[0]):
            subject_list.append("TCIA")
        for i in range(df_TCGA.shape[0]):
            subject_list.append("TCGA")

        df_count = pd.DataFrame(columns=['Source'],
                                   data=subject_list)

        return df_count

    elif type == "GBM_TYPE":
        df_type = pd.DataFrame()
        df_type['GBM Type'] = df_TCGA.apply(lambda row: 'Primary' if (row['IDH1'] == 0) else 'Secondary', axis=1)

        return df_type




"""
Find TCIA-TCGA matches.
"""
def find_matches(df_TCIA, df_TCGA):
    # Make lists with original data
    TCGA_subjects = df_TCGA.index.tolist()
    TCIA_subjects = df_TCIA.index.tolist()

    # Initialise new lists
    matches = []
    subjects_excluded = []

    # Split data
    for TCGA_subject in TCGA_subjects:
        if TCGA_subject in TCIA_subjects:
            matches.append(TCGA_subject)
        else:
            subjects_excluded.append(TCGA_subject)

    return matches



"""
Run PCA
Input: (data, dimensions)
Returns: Numpy array
NB: Input is expected to already have been normalised/standardised 
"""
def run_pca(data, matches, dimensions):
    pca = PCA(n_components=dimensions)                    # Number of components to keep
    # NB: "fit" and "transform" is same as "fit_transform"
    pca_results = pca.fit_transform(data)                 # Fit model and Apply dimensionality reduction
    if dimensions == 2:
        df = pd.DataFrame(data=pca_results, index=matches, columns=['x','y'])
    elif dimensions == 3:
        df = pd.DataFrame(data=pca_results, index=matches, columns=['x', 'y','z'])
    else:
        df = pd.DataFrame(data=pca_results, index=matches) #Just get custom column names
    return df



def make_scree_and_cumul_from_standardised_data(stand_data):
    # Eigen_vals + eigen_vectors
    cor_mat1 = np.corrcoef(stand_data.T)

    # Use "eigh" to avoid imaginary numbers
    eig_vals, eig_vecs = np.linalg.eigh(cor_mat1)

    # Make a list of (eigenvalue, eigenvector) tuples
    eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:, i]) for i in range(len(eig_vals))]

    # Sort the (eigenvalue, eigenvector) tuples from high to low
    eig_pairs.sort(key=lambda tup: tup[0])
    eig_pairs.reverse()

    # Calculate variance
    tot = sum(eig_vals)
    var_exp = [(i / tot) * 100 for i in sorted(eig_vals, reverse=True)]
    cum_var_exp = np.cumsum(var_exp)

    # Get plot
    return plotly_scree(var_exp, cum_var_exp)


"""
Plotly Satter: return data to plot
"""
def plotly_scatter(coords, y_labels, names):
    traces = []
    # The names are SUPER IMPORTANT!!!!
    for name in names:
        trace = Scatter(
            x=coords[y_labels == name, 0],  # y_labels = primary or secondary
            y=coords[y_labels == name, 1],
            mode='markers',
            name=name,
            marker=Marker(
                size=12,
                line=Line(
                    color='rgba(217, 217, 217, 0.14)',
                    width=0.5),
                opacity=0.8))
        traces.append(trace)
    return traces





"""
For Dash. Makes the "other" categories from drop-down
"""
def helper_make_other_df(df, selected):

    # Check if anything from drop-down selected
    if not selected:
        new_df = pd.DataFrame(data=[1]*len(list(df.index)), index=df.index, columns=['All IDs'])

    # If genes selected
    else:
        new_df = pd.DataFrame(df[selected])
        new_df['Other IDs'] = new_df.sum(axis=1)
        new_df['Other IDs'] = np.where(new_df["Other IDs"] == 0, 1, 0)

        # Switch order of columns
        cols = new_df.columns.tolist()
        cols = cols[::-1]
        new_df = new_df[cols]


    return new_df



"""
Plotly Scree: returns data for scree (and cumulative variance)
HARDCODED NUMBER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
"""
def plotly_scree(var_exp = [], cum_var_exp = []):
    if len(cum_var_exp) == []:
        trace1 = Bar(
            x=['PC %s' % i for i in range(1, 51)],
            y=var_exp,
            marker=dict(
                color=LIGHT_BLUE,
            ),
            showlegend=False)
        return Data([trace1])
    elif var_exp == []:
        trace2 = Bar(
            x=['PC %s' % i for i in range(1, 51)],
            y=cum_var_exp,
            marker=dict(
                color=NAVY,
            ),
            name='cumulative explained variance')
        return Data([trace2])
    else:
        trace1 = Bar(
            x=['PC %s' % i for i in range(1, 51)],
            y=var_exp,
            marker=dict(
                color=NAVY,
            ),
            showlegend=False)

        trace2 = Scatter(
            x=['PC %s' % i for i in range(1, 51)],
            y=cum_var_exp,
            marker=dict(
                color=LIGHT_BLUE,
            ),
            name='cumulative explained variance')
        return Data([trace1, trace2])               # both in one plot
                         # Just scree plot





"""
Cumultative variance as well
"""
def plotly_scree_CCA(var_exp, cum_var_exp = []):
    if len(cum_var_exp) != 0:
        trace1 = Bar(
            x=['CC pair %s' % i for i in range(1, 51)],
            y=var_exp,
            showlegend=False)

        trace2 = Scatter(
            x=['CC pair %s' % i for i in range(1, 51)],
            y=cum_var_exp,
            name='cumulative explained variance')
        return Data([trace1, trace2])               # both in one plot
    else:
        trace1 = Bar(
            x=['CC pair %s' % i for i in range(1, 51)],
            y=var_exp,
            showlegend=False,
            marker=dict(
                color=NAVY,
                ))

        return Data([trace1])                       # Just scree plot




"""
Only Bar
"""
def plotly_rcca_scree(cor):
    trace1 = Bar(
        x=[i for i in range(1, 51)],
        y=cor,
        showlegend=False)
    return Data([trace1])



"""
run t-sne: return --coordinates-- to be plotted or used in computations
"""
def run_tsne(data, matches, dimensions, perplexity, learning_rate):
    tsne = TSNE(n_components=dimensions,
                verbose=0,                  # Control printing
                perplexity=perplexity,
                learning_rate=learning_rate)
    tsne_results = tsne.fit_transform(data)
    if (dimensions== 2):
        df = pd.DataFrame(data=tsne_results, index=matches, columns=['x', 'y'])
    if (dimensions== 3):
        df = pd.DataFrame(data=tsne_results, index=matches, columns=['x', 'y','z'])
    return df


"""
Return distance matrix as DataFrame
NB: For now, only works on 2D tsne results
"""
def tsne_dist_matrix(df_tsne_results):
    df_dist_matrix = pd.DataFrame(distance_matrix(df_tsne_results.values,          # Create distance matrix (DataFrame)
                                                  df_tsne_results.values),
                                  index=df_tsne_results.index,
                                  columns=df_tsne_results.index)
    return df_dist_matrix


"""
Returns smallest distance + corresponding IDs
"""
def smallest_dist(df_dist_matrix):
    # Initialise
    smallest_dist = df_dist_matrix.iloc[1, 0]
    IDs = [df_dist_matrix.index[1], df_dist_matrix.columns[0]]

    # iterate through DataFrame
    for index_row, row in df_dist_matrix.iterrows():
        for index_column, dist in row.iteritems():
            if index_row != index_column:
                if dist < smallest_dist:
                    smallest_dist = dist
                    IDs = [index_row, index_column]

    return smallest_dist, IDs





"""
add info, build new dataframe with additional columns
"""
def add_info(data, list_new_columns, list_new_names):
    i = 0
    for name in list_new_names:
        data[name] = list_new_columns[i]
        i += 1

    return data


"""
From list, make binary DF with primary and secondary GBM
"""
def make_bin_df(data):
    df = pd.DataFrame()
    accumulator = []

    for unique in np.unique(data):
        for item in data:
            if item == unique:
                accumulator.append(1)
            else:
                accumulator.append(0)

        df[unique] = accumulator
        accumulator =[]

    return df



"""
Get sorted eigen pairs
"""
def eigen_pairs(X_std):
    # Eigen_vals + eigen_vectors
    cor_mat1 = np.corrcoef(X_std.T)

    # Use "eigh" to avoid imaginary numbers
    eig_vals, eig_vecs = np.linalg.eigh(cor_mat1)

    # Make a list of (eigenvalue, eigenvector) tuples
    eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:, i]) for i in range(len(eig_vals))]

    # Sort the (eigenvalue, eigenvector) tuples from high to low
    eig_pairs.sort(key=lambda tup: tup[0])
    eig_pairs.reverse()
    return eig_pairs, eig_vals, eig_vecs



"""
K closests based on distance matrix
"""

def k_closest(data, k):

    # Transpose so you can use "nsmallest" more intuitively
    X = data.T

    df = pd.DataFrame(index=data.index, columns=[i for i in range(1, k + 1)])

    for i in X.index:
        Y = X.nsmallest(k + 1, i, keep='first')
        Y = Y.T
        Y = Y[Y.index.str.startswith(i)]
        if i in list(Y): # Drop the subject itself
            Y = Y.drop(i, axis=1, inplace=False)
        if len(list(Y)) > k:    # in the case with only 9 genes, many are subjects are the same!!! so make sure you only get clusters of size k back
            Y = Y.T[:k].T # Hack. :)
        df.loc[i] = Y.columns

    return df



"""
tsne_score based on neighbours. Tolerates threshold
"""
def tsne_score_neighbours(ground_truth, imaging):
    total_possible = 0
    count_corrects = 0
    k = len(ground_truth.columns)

    for index_row, row in ground_truth.iterrows():

        binary_mask = []

        for i in list(row):
            if i == 'nan':
                binary_mask.append(1)
            else:
                binary_mask.append(0)
        num_nans = sum(binary_mask)

        for index_column, subject in row.iteritems():

            if subject != "nan":
                total_possible += 1
                img_row = list(imaging.loc[index_row,:])
                if subject in img_row[:k-num_nans]: # Only look at those k MINUS num_nans closests
                    count_corrects += 1

    score = (1 - (count_corrects / total_possible))

    return score, count_corrects, total_possible



"""
tsne_score based on distances
"""
def tsne_score_distances(ground_truth, imaging, ):

    sum_distances = 0
    # Loop through ground truth matrix
    for index_row, row in ground_truth.iterrows():
        for index_column, subject in row.iteritems():
            sum_distances += imaging.loc[index_row][subject]

    return sum_distances


"""
get primary secondary from idh1
"""
def get_prim_sec_from_idh1(tcga):
    df = get_countplot_data('GBM_TYPE', df_TCGA=tcga)
    df_prim_sec = make_bin_df(df['GBM Type'].values)
    return df_prim_sec


"""
Dictionary for dash_tsne
"""
def dash_tsne():
    dict_tsne = {}
    for pc in PCs:
        for dim in tsne_dims:
            for perp in perps:
                for learn in learn_rates:
                    dict_tsne["PCs{}Dim{}Perp{}Learn{}".format(pc, dim, perp, learn)] = pd.read_csv(
                        dash_path + "tsne_PCs{}_dim{}_perp{}_learnRate{}".format(pc, dim, perp, learn))
                    dict_tsne["PCs{}Dim{}Perp{}Learn{}".format(pc, dim, perp, learn)].set_index(
                        dict_tsne["PCs{}Dim{}Perp{}Learn{}".format(pc, dim, perp, learn)].columns[0],
                        inplace=True)

    return dict_tsne


"""
Dictionary for dash_pca
"""
def dash_pca_scree():
    dict_pca = {}
    TCIA = download_directly("TCIA_clean")
    X = TCIA.values
    dict_pca["X_std"] = StandardScaler().fit_transform(X)
    return dict_pca


"""
Cleans data according to threshold (set to >3)
"""
def create_gene_data_for_survival_rf(threshold):
    df = pd.read_csv(TCGA_path)
    df.set_index(['mutation'], inplace=True, drop=True)
    df['sum_rows'] = df.sum(axis=1)
    df = df.drop(df[df.sum_rows <= threshold].index)
    df.drop(['sum_rows'], inplace=True, axis=1)
    return df.T


"""
Create columns of days, dead/alive, and even (1 for dead)
"""
def create_survival_columns():
    df = pd.read_csv(survival_path)
    df.set_index('submitter_id', drop=True, inplace=True)
    df.dropna(inplace=True)
    df['Status'] = df.apply(lambda row: 1 if (row['vital_status'] == 'dead') else 0, axis=1)
    return df


def get_gene_groups():
    df = pd.read_csv(gene_group_path)
    df.set_index('ID', inplace=True, drop=True)
    df.drop('Analysis', inplace=True, axis=1)
    df.drop('Score', inplace=True, axis=1)
    df.drop('Focus Molecules', inplace=True, axis=1)
    df.drop('Top Diseases and Functions', inplace=True, axis=1)

    ID_START = 1
    dict = {}
    for i in range(ID_START, len(df.index) + 1):
        dict[str(i)] = list(df.loc[i])[0].split(';')

    return dict


def load_dist_matrix_only_matches():
    df = pd.read_csv(dist_matrix_path)
    df.set_index('Unnamed: 0', inplace=True, drop=True)

    # Download 57 subjects that were matched TCGA to TCIA
    TCGA_CLEAN = download_directly("TCGA_CLEAN")

    # only keep matches
    for index, row in df.iterrows():
        if index not in TCGA_CLEAN.index:
            df.drop(index, inplace=True)

    df = df.T
    for index, row in df.iterrows():
        if index not in TCGA_CLEAN.index:
            df.drop(index, inplace=True)

    return df



def surv_TCIA_TCGA_matches():
    df_surv = pd.read_csv("/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/Bakas57subjects/tcga_survival_data.csv")
    df_surv.set_index('submitter_id', inplace=True, drop=True)

    df_matches = download_directly("TCGA_clean")

    for x in df_surv.index:
        if x not in df_matches.index:
            df_surv = df_surv.drop(x)

    df_surv.dropna(inplace=True)

    return df_surv

def only_x_y_z(df):

    if df.columns[2] == 'z':
        df_new = df[['x', 'y', 'z']]
    else:
        df_new = df[['x', 'y']]

    return df_new



"""
Based on BRaTS_TCIA matches, make nested dictionary to hold paths to images
"""
def nested_dict_folder_paths():
    path = '/Users/kimrants/Desktop/Thesis/HGG/'
    mapping = download_data("BRATS_TCIA_MATCHES")

    # Count to check correctness
    num_subjects = 0

    # Dicts to store paths
    dict_outer = {}

    # Loop through files
    for index, row in mapping.iterrows():
        for directory in os.listdir(path):
            if row[0] in directory:
                num_subjects += 1

                # Create paths that can be looped through
                path_segmentation = path + directory + "/" + directory + "_" + "seg" + ".nii.gz"
                path_t1 = path + directory + "/" + directory + "_" + "t1" + ".nii.gz"
                path_t1c = path + directory + "/" + directory + "_" + "t1ce" ".nii.gz"
                path_flair = path + directory + "/" + directory + "_" + "flair" + ".nii.gz"
                path_t2 = path + directory + "/" + directory + "_" + "t2" + ".nii.gz"

                dict_inner = {}
                dict_inner['Segmentation'] = path_segmentation
                dict_inner['T1'] = path_t1
                dict_inner['T1c'] = path_t1c
                dict_inner['FLAIR'] = path_flair
                dict_inner['T2'] = path_t2

                dict_outer[index] = dict_inner

    return num_subjects, dict_outer


"""
Extract features based on nested dict input with folder paths
"""
def nested_dict_features(extractor, dict_paths):

    dict_features_outer = {}
    for key, value in dict_paths.items():

        dict_features_inner = {}
        dict_features_inner['T1C_NECROSIS'] = extractor.execute(value['T1c'],
                                                        value['Segmentation'],
                                                        label=NECROTIC)

        dict_features_inner['FLAIR_EDEMA'] = extractor.execute(value['FLAIR'],
                                                                value['Segmentation'],
                                                                label=EDEMA)

        dict_features_inner['T1C_ENHANCING'] = extractor.execute(value['T1c'],
                                                               value['Segmentation'],
                                                               label=ENHANCING_TUMOR)

        dict_features_outer[key] = dict_features_inner

    return dict_features_outer




def make_replication_study_df(dict_features):

    mapping = download_data("BRATS_TCIA_MATCHES")
    mapping.index.names = ['ID'] # To allows matching with other places

    # make df
    df =  pd.DataFrame(index=mapping.index, columns=['NCR',
                                                     'ED',
                                                     'ET',
                                                     'TC',
                                                     'WT',
                                                     'NCR / ET',
                                                     'ET / TC',
                                                     'ET / WT',
                                                     'NCR / WT',
                                                     'ED / WT',
                                                     'TC / WT'])

    # loop through triple nested dict (Subject:type:feature) to get values
    for key, value in dict_features.items():
        df.loc[key, 'NCR'] = value['NCR']['original_shape_Volume']
        df.loc[key, 'ED'] = value['ED']['original_shape_Volume']
        df.loc[key, 'ET'] = value['ET']['original_shape_Volume']

    # Add sums
    df['TC'] = df['NCR'] + df['ET']
    df['WT'] = df['NCR'] + df['ET'] + df['ED']

    # Add ratios
    df['NCR / ET'] = df['NCR'] / df['ET']
    df['ET / TC'] = df['ET'] / df['TC']
    df['ET / WT'] = df['ET'] / df['WT']
    df['NCR / WT'] = df['NCR'] / df['WT']
    df['ED / WT'] = df['ED'] / df['WT']
    df['TC / WT'] = df['TC'] / df['WT']

    # Turn into floats
    df = df.astype(float)

    return df






"""
Return dataFrame with subjects as index, columns as gene groups (with count of mutations)
"""

def df_gene_groups():
    df_TCGA = pd.read_csv(TCGA_path)  # (17107, 394)
    df_TCGA = df_TCGA.set_index('xmutation')
    df_TCGA = df_TCGA.T
    cleaned_dict_gene_groups = np.load('/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Input_Data/cleaned_dict_gene_groups.npy').item()

    df = pd.DataFrame(index=df_TCGA.index, columns=list(cleaned_dict_gene_groups.keys()))

    for subject, row in df.iterrows():
        for gene_group, nan in row.iteritems():
            df.loc[subject, gene_group] = df_TCGA.loc[subject,
                                                      cleaned_dict_gene_groups[gene_group]].sum()

    return df


def marker_size(vals):
    # If NOT gene group
    if len(vals) == 0:
        size = 15
    else:
        # Avoid zero division
        if max(vals) != 0:
            size = [ (x / max(vals)) * 30 + 10 for x in vals]
        else:
            size = 15
    return size




"""
Create nan values in closests matrix above some threshold
"""
def apply_threshold_on_k_closest(df, dist_matrix, thres):
    for focus_subject, row in df.iterrows():
        for index_column, other_subject in row.iteritems():
            if focus_subject != "nan":
                if other_subject != "nan":
                    if dist_matrix.loc[focus_subject, other_subject] > thres:
                        df.loc[focus_subject,index_column] = "nan"

    return df


"""
As above, but with allowed threshold as well
"""

def tsne_optimal_hyperparameters(ks=[5],
                                thresholds=[12],
                                tsne_dims=[2],
                                PCs=[13],
                                perps=[50],
                                learn_rates=[200],
                                subjects=None,
                                path=None):

    data = []
    score = 999999999
    for k in ks:
        for thres in thresholds:
            for dim in tsne_dims:
                for pc in PCs:
                    for perp in perps:
                        for l in learn_rates:

                            # Download genomic distance matrix from Paul (all genes)
                            dist_matrix_gene = pd.read_csv(dist_matrix_path)
                            dist_matrix_gene.set_index('Unnamed: 0', inplace=True, drop=True)

                            # Pick only selected subjects (30, 35, 53 or 57 subjects)
                            dist_matrix_gene = dist_matrix_gene[subjects]
                            dist_matrix_gene = dist_matrix_gene.T[subjects]

                            # Identify k closests neighbours for each data point in Genomic Space; this is ground truth
                            k_closest_genomic = k_closest(dist_matrix_gene, k)

                            # Apply Threshold
                            thres_k_closest_genomic = apply_threshold_on_k_closest(k_closest_genomic, dist_matrix_gene,
                                                                                   thres)

                            # Create file path for DOWNLOADING t-SNE
                            file = "tsne_PCs" + str(pc) + "_dim" + str(dim) + "_perp" + str(perp) + "_learnRate" + str(
                                l)

                            # Read in t-SNE plot
                            df = pd.read_csv(path + file)
                            df_img_coord = df.set_index('ID', drop=True)

                            # Get only the coordinates in file (the file also contains gene groups, etc. for DASH)
                            df_img_coord = only_x_y_z(df_img_coord)

                            # Keep only subjects
                            df_img_coord = df_img_coord.T[subjects].T

                            # Get distance matrix
                            dist_matrix_img = tsne_dist_matrix(df_img_coord)

                            # Get k closests neighbours pr. ID
                            k_closest_image = k_closest(dist_matrix_img, k)

                            # Get score by comparing, score is an error... so it has be minimised!
                            temp_score, corrects, total = tsne_score_neighbours(thres_k_closest_genomic, k_closest_image)

                            if temp_score < score:
                                score = temp_score  # Needed because we enter w/ score = 999999
                                dict_opt_param = {}
                                dict_opt_param['score'] = score
                                dict_opt_param['corrects'] = corrects
                                dict_opt_param['total'] = total
                                dict_opt_param['k'] = k
                                dict_opt_param['thres'] = thres
                                dict_opt_param['dim'] = dim
                                dict_opt_param['pc'] = pc
                                dict_opt_param['perp'] = perp
                                dict_opt_param['learnRate'] = l

                            data.append([temp_score, dim, pc, perp, l, k, thres, corrects, total])

    return dict_opt_param, data






"""
Compute tSNE for plot in Dash 
"""


def Dash_tsne_no_PCA(df, add_columns, save_path):

    # Standardise input
    X_std = StandardScaler().fit_transform(df.values)

    for dim in tsne_dims:
        for perp in perps:
            for learn_rate in learn_rates:
                results = run_tsne(data=X_std,
                                     matches=df.index,
                                     dimensions=dim,
                                     perplexity=perp,
                                     learning_rate=learn_rate)

                results = add_info(results,
                                   add_columns.T.values.tolist(),
                                   add_columns.columns)

                results.to_csv(save_path+
                       "tsne" +
                       "_PCsNO" +
                       "_dim" + str(dim) +
                       "_perp" + str(perp) +
                       "_learnRate" + str(learn_rate))

def Dash_tsne_with_PCA(df, add_columns, save_path):

    # Standardise input
    X_std = StandardScaler().fit_transform(df.values)

    for dim in tsne_dims:
        for perp in perps:
            for learn_rate in learn_rates:
                results = run_tsne(data=X_std,
                                     matches=df.index,
                                     dimensions=dim,
                                     perplexity=perp,
                                     learning_rate=learn_rate)

                results = add_info(results,
                                   add_columns.T.values.tolist(),
                                   add_columns.columns)

                results.to_csv(save_path+
                       "tsne" +
                       "_PCs" + str(len(df.columns)) +
                       "_dim" + str(dim) +
                       "_perp" + str(perp) +
                       "_learnRate" + str(learn_rate))