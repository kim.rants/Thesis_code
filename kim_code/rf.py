import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from lifelines.estimation import KaplanMeierFitter
import matplotlib.pyplot as plt
import plotly.plotly as py
import plotly.tools as tls
from plotly.graph_objs import *


"""
    Create random forrest based on mutation data and survival time
    TO-DO: maybe save the file formatted so you can just open it as is?
"""

# Set this to decide which analysis to perform
do_survival = False
num_top_features = 10

if do_survival:
    do_random_forrest = False
else:
    do_random_forrest = True


def main():
############################## GET DATA ###########################################

    # Data paths
    mutation_data = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/gbm-mutations/Data/TCGA.GBM.mutect.somaticgene_matrix.csv'
    survival_data = '/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/gbm-mutations/Data/tcga_survival_data.csv'

    # Download mutation data and transpose
    df, df_survival = get_data(mutation_data, survival_data)

############################## Clean data and create one DataFrame ###########################################
    df = df.transpose()  # mutations as columns, subjects (people) as rows
    print("df is: ", df.shape)

    # Create new header
    new_header = df.iloc[0] #grab the first row for the header
    df = df[1:] #take the data less the header row
    df.columns = new_header #set the header row as the df header

    # Survival Data
    df_survival = df_survival.set_index('submitter_id') # set index (i.e. row)
    df_survival = df_survival.dropna(axis=0, how='any', thresh=None, subset=None, inplace=False)
    print("df_survival is: ", df_survival.shape)

    # Create new column
    # Set dead = event = 1, alive = 0, allowing for censoring
    df_survival['Event'] = np.where(df_survival['vital_status']=='dead', 1, 0)

    # Find matches and join
    rf_data = df.join(df_survival)

    if(do_survival):
        num_matches = rf_data.shape[0]
        print("Num matches is: ", num_matches)
        survival_analysis(rf_data)
    else:
        rf_data = rf_data.dropna(axis=0, how='any', thresh=None, subset=None, inplace=False) # Drop where there is not a match
        rf_data = rf_data[rf_data.vital_status != 'alive']  # only keep subjects that are "dead", this removes almost 100 people
        num_matches = rf_data.shape[0]
        print("Num matches is: ", num_matches)
        feature_names = new_header
        predictions, ranked_features = random_forrest(rf_data, feature_names)
        print("Top {} features ranked".format(num_top_features))
        print(ranked_features[:num_top_features-1])


"""
What to do with people that are not dead?
--> In Random Forrest I currently drop them
"""

"""
get_data: Gets data based on specified paths. 
"""
def get_data(mutation_data, survival_data):

    # mutation data
    df = pd.read_csv(mutation_data)

    # Survival data
    df_raw_survival = pd.read_csv(survival_data)

    return df, df_raw_survival


"""
survival_analysis: Will use KaplanMeierFitter to develop survival analysis plot
"""
def survival_analysis(rf_data):
    kmf = KaplanMeierFitter()
    # Censor data
    kmf.fit(rf_data['days_to_last_follow_up'], event_observed=rf_data['Event'])
    kmf.plot(ci_force_lines=True, title='Survival Analysis (95% CI)')
    kmf1 = plt.gcf()
    pyplot(kmf1, legend=False)



"""
random_forrest: predicts survival and returns top features
"""
def random_forrest(rf_data, feature_names):
    # Independent and dependent variables
    X = rf_data.drop(['days_to_last_follow_up', 'vital_status', 'Event'], axis=1)
    Y = rf_data['days_to_last_follow_up']

    # Split data into training and test data
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.3)

    # Train Model
    rfr = RandomForestRegressor(n_estimators=50, max_depth=None)  # rfr= randomForrestRegressor
    rfr.fit(x_train, y_train)

    # Get predictions
    rfr_prediction = rfr.predict(x_test)

    ranked_features = sorted(zip(map(lambda x: round(x, 4), rfr.feature_importances_), feature_names), reverse=True)

    return rfr_prediction, ranked_features



"""
pyplot: Make interactive graph
"""
def pyplot(fig, ci=True, legend=True):
    # Convert mpl fig obj to plotly fig obj, resize to plotly's default
    py_fig = tls.mpl_to_plotly(fig, resize=True)

    # Add fill property to lower limit line
    if ci == True:
        style1 = dict(fill='tonexty')
        # apply style
        py_fig['data'][2].update(style1)

        # Change color scheme to black
        py_fig['data'].update(dict(line=Line(color='blue')))

    # change the default line type to 'step'
    py_fig['data'].update(dict(line=Line(shape='hv')))
    # Delete misplaced legend annotations
    py_fig['layout'].pop('annotations', None)

    if legend == True:
        # Add legend, place it at the top right corner of the plot
        py_fig['layout'].update(
            showlegend=True,
            legend=Legend(
                x=1.05,
                y=1
            )
        )

    # Send updated figure object to Plotly, show result in notebook
    return py.iplot(py_fig)


if __name__ == '__main__':
    main()