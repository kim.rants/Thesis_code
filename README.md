# Data exploration for understanding glioblastoma through radiogenomics

This Gitlab account includes the Jupyter Notebooks used for producing the above project:
"Data exploration for understanding glioblastoma through radiogenomics", Imperial College London 2018


## Overview

There are three main folders: i) kim_code, 2) input_data, and 3) output_data


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 


### Prerequisites

What things you need to install the software and how to install them

```
- We recommend using Anaconda
```

```

### Folders

Kim_code includes the Dashboard that has been created to explore t-SNE. It also includes the script "radiogenomics", which was specifically built as a supporting script.
Input_data holds the input data for the project. Output_data is the output data, which primarily consists of thousands of t-SNE plots.




## Authors

* **Kim Rants** 
* **Elsa Angelini** 



## Acknowledgments

Elsa Angelini
Paul Blakeley
Jazz Smith
Jeremy Cohen
