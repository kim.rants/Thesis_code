library('pheatmap')
library('RColorBrewer')

#insert file name here
fname<-"GBM_mutation_L1dist_matrix2.csv"

#read data rpkm table
rpkm<-read.table(fname, header=TRUE, sep=",", check.names=FALSE)
rowlen=ncol(rpkm)

#set rownames
rpkm2<-as.matrix(rpkm[,2:rowlen])
rownames(rpkm2)<-rpkm[,1]

#generate heatmap
hmobj<-pheatmap(rpkm2, cellwidth=1, 
         cluster_rows=FALSE,
         cluster_cols=FALSE,
        # clustering_distance_rows = "manhattan", 
         #clustering_distance_cols = "manhattan",
         scale='none',
         fontsize_col=1,
         fontsize_row=1,
         cellheight = 1
)
