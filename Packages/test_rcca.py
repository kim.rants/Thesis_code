import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import numpy as np
import sys
sys.path.append("/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/Packages/")
import rcca
from sklearn.model_selection import train_test_split


"""
TCGA DATA
"""
# TCGA mutation data
df_TCGA = pd.read_csv('/Users/kimrants/PycharmProjects/thesis_code/Thesis_code/gbm-mutations/Data/TCGA.GBM.mutect.somaticgene_matrix.csv')

# Transpose the data
df_TCGA = df_TCGA.transpose()

new_header = df_TCGA.iloc[0] #grab the first row for the header
df_TCGA = df_TCGA[1:] #take the data less the header row
df_TCGA.columns = new_header #set the header row as the df header
df_TCGA.index.names = ['ID']


"""
TCIA DATA
"""
# TCIA feature data
df_TCIA = pd.read_csv('/Users/kimrants/Desktop/DATA/TCGA_GBM_radiomicFeatures.txt')

# Set index
df_TCIA = df_TCIA.set_index('ID')

# Remove date column
df_TCIA = df_TCIA.drop(['Date'], axis=1)

# Drop NaN columns
df_TCIA= df_TCIA.dropna(axis=1, how='any', thresh=None, subset=None, inplace=False)



# Find overlap
TCGA_subjects = df_TCGA.index.tolist()
TCIA_subjects = df_TCIA.index.tolist()

overlaps = []
to_be_excluded = []
for TCGA_subject in TCGA_subjects:
    if TCGA_subject in TCIA_subjects:
       overlaps.append(TCGA_subject)
    else:
       to_be_excluded.append(TCGA_subject)


df_TCGA_only_matches = pd.DataFrame(data=df_TCGA, index =overlaps)
df_TCIA_only_matches = pd.DataFrame(data=df_TCIA, index =overlaps)

# Make header pretty
df_TCGA_only_matches.index.names=['ID']
df_TCIA_only_matches.index.names=['ID']
df_TCIA_only_matches.columns.names= ['Features']
df_TCGA_only_matches.columns.names= ['Mutations']



# get primary and secondary again with ID as index
df_type_type =pd.DataFrame()
df_type_type['GBM Type'] = df_TCGA_only_matches.apply(
    lambda row: 'Primary' if (row['IDH1']==0) else 'Secondary',
    axis=1
)

#Merge
df_TCIA_matches_and_class = df_TCIA_only_matches.join(df_type_type)


# y-label: Primary vs. secondary GBM
# X-label: all imaging features
X = df_TCIA_matches_and_class.drop(axis=1, labels='GBM Type').values # Everything but class, just values
y = df_TCIA_matches_and_class['GBM Type'].values # Just the class, just values



X_std = StandardScaler().fit_transform(X)

# function, pass in x_std as data!
def run_pca(data, dimensions):
  pca = PCA(n_components=dimensions)
  pca.fit(data)
  pca_results = pca.transform(data)
  return pca_results




# Imaging
# pca reduced dimensions as X-block
pca_imaging = run_pca(X_std, 50)


# Genomic
#  Mutation  sta-tus  of  the  9  relevant  genes  in  glioblastom ?? Not mentioned in report elsewhere..
# (TP53,  PTEN,  NF1,  EGFR,  IDH1,  PIK3R1,  RB1,  PIK3CA, and  PDGFRA) as Y-block
df_relevant_genes = df_TCGA_only_matches[['TP53',
                                          'PTEN',
                                          'NF1',
                                          'EGFR',
                                          'IDH1',
                                          'PIK3R1',
                                          'RB1',
                                          'PIK3CA',
                                          'PDGFRA']].copy()
df_relevant_genes = df_relevant_genes.values


"""
rCCA
"""



nCanComponents = min(pca_imaging.shape[1],
                    df_relevant_genes.shape[1])

print(nCanComponents)

# Split into training and validation data
TCIA_train, TCIA_test = train_test_split(pca_imaging, test_size=0.2)
TCGA_train, TCGA_test = train_test_split(df_relevant_genes, test_size=0.2)

#print(TCIA_train.shape)
#print(TCGA_train.shape)
#print(TCIA_train)
#print(TCGA_train)

# Create a cca object as an instantiation of the CCA object class.
cca = rcca.CCA(kernelcca = False, reg = 0,
               verbose=True,
               numCC = nCanComponents)

# Train
cca.train([TCIA_train, TCGA_train])

# Test scores
testcorrs = cca.validate([test1, test2])

print(testcorrs)
